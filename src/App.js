import Game from "./components/Game";

import './styles/style.scss';

const App = () => {
    return (
      <div className="app">
        <Game/>
      </div>
    );
};

export default App;